#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>

using namespace std;

void crearArchivoUser(){

	string nameFile = "data/usuario.txt";
	struct usuario u[100];

	u[0].inicializar("0001","INDUSTRIA TEXTIL AMAZONAS SAC","20543297411","ADMIN",encriptaClave("123456"));
	u[1].inicializar("0002","LOPE RIVERA HECTOR","10040534208","PROVEEDOR",encriptaClave("10040534208"));
	u[2].inicializar("0003","ENTEL PERU S.A.","20106897914","PROVEEDOR",encriptaClave("20106897914"));
	u[3].inicializar("0004","ENEL DISTRIBUCION PERU S.A.A.","20269985900","PROVEEDOR",encriptaClave("20269985900"));
	u[4].inicializar("0005","TELEFONICA DEL PERU S.A.A","20100017491","PROVEEDOR",encriptaClave("20100017491"));
	u[5].inicializar("0006","GRIFO TRAPICHE SRL","20193386220","PROVEEDOR",encriptaClave("20193386220"));
	u[6].inicializar("0007","HOMECENTERS PERUANOS S.A","20536557858","PROVEEDOR",encriptaClave("20536557858"));
	u[7].inicializar("0008","REPSOL COMERCIAL SAC","20503840121","PROVEEDOR",encriptaClave("20503840121"));
	u[8].inicializar("0009","PE�A  DE SALDARRIAGA  LUZMILA GRISELDA","10003224428","PROVEEDOR",encriptaClave("10003224428"));
	u[9].inicializar("0010","HUERTA ARIZA WALABONZO","10094813293","PROVEEDOR",encriptaClave("10094813293"));
	u[10].inicializar("0011","INDUSTRIA TEXTIL PIURA S.A.","20102728743","PROVEEDOR",encriptaClave("20102728743"));
	u[11].inicializar("0012","CONSORCIO MORENO S.A.C.","20477552804","PROVEEDOR",encriptaClave("20477552804"));
	u[12].inicializar("0013","CONSORCIO YAHVE S.A.C.","20559749347","PROVEEDOR",encriptaClave("20559749347"));
	u[13].inicializar("0014","COPY BORAL S.A.C.","20600504844","PROVEEDOR",encriptaClave("20600504844"));
	u[14].inicializar("0015","EMPRESA DE TRANSPORTE TURISTICO OLANO S.A.","20135414931","PROVEEDOR",encriptaClave("20135414931"));
	u[15].inicializar("0016","ALBAN FACHO EDITHA ELIZABETH","10436479447","PROVEEDOR",encriptaClave("10436479447"));
	u[16].inicializar("0017","CHIRA  TARCILLA  CESAR AUGUSTO","10772284417","PROVEEDOR",encriptaClave("10772284417"));
	u[17].inicializar("0018","REPRESENTACIONES GENERALES PERU S.A.","20215702813","PROVEEDOR",encriptaClave("20215702813"));
	u[18].inicializar("0019","HUERTA AGUIRRE ALEJANDRO","10323008545","PROVEEDOR",encriptaClave("10323008545"));
	u[19].inicializar("0020","MAPFRE PERU S.A. EPS","20517182673","PROVEEDOR",encriptaClave("20517182673"));
	u[20].inicializar("0021","TECNIFAJAS S.A.","20100244714","PROVEEDOR",encriptaClave("20100244714"));
	u[21].inicializar("0022","TORRES  QUISPE  CRISOSTOMO","10401965489","PROVEEDOR",encriptaClave("10401965489"));
	u[22].inicializar("0023","SIENA SERVICIOS SRL","20268418955","PROVEEDOR",encriptaClave("20268418955"));
	u[23].inicializar("0024","HOMECENTERS PERUANOS S.A","20536557858","PROVEEDOR",encriptaClave("20536557858"));
	u[24].inicializar("0025","ANDEAN GARDEN E.I.R.L","20512745041","PROVEEDOR",encriptaClave("20512745041"));
	u[25].inicializar("0026","ROMERO ASCAYO GIOVANNA","10443729980","PROVEEDOR",encriptaClave("10443729980"));
	u[26].inicializar("0027","FIERRO CENTRO SAC","20509264512","PROVEEDOR",encriptaClave("20509264512"));
	u[27].inicializar("0028","TRAILERGAS S.A.C","20536606573","PROVEEDOR",encriptaClave("20536606573"));
	u[28].inicializar("0029","AC INDUSTRIA GRAFICA S.R.L.","20549921575","PROVEEDOR",encriptaClave("20549921575"));
	u[29].inicializar("0030","CORPORACION METALURGICO AGUILAR E.I.R.L.","20550733090","PROVEEDOR",encriptaClave("20550733090"));
	u[30].inicializar("0031","E & N ELECTRIC S.A.C.","20557676881","PROVEEDOR",encriptaClave("20557676881"));
	u[31].inicializar("0032","ESTACION DE SERVICIO EL TRIANGULO SAC","20509145424","PROVEEDOR",encriptaClave("20509145424"));
	u[32].inicializar("0033","FILASUR S.A.","20378092419","PROVEEDOR",encriptaClave("20378092419"));
	u[33].inicializar("0034","JAS IMPORT & EXPORT SRL","20338048905","PROVEEDOR",encriptaClave("20338048905"));
	u[34].inicializar("0035","HUAMANI GASPAR GUILLERMO SANTOS","10105536254","PROVEEDOR",encriptaClave("10105536254"));
	u[35].inicializar("0036","PC LIKE SAC.","20552419023","PROVEEDOR",encriptaClave("20552419023"));
	u[36].inicializar("0037","LOPE RIVERA HECTOR","10040534208","PROVEEDOR",encriptaClave("10040534208"));
	u[37].inicializar("0038","DMP ETIQUETAS SAC","20451450264","PROVEEDOR",encriptaClave("20451450264"));
	u[38].inicializar("0039","TEXTIL CHOTANO SOCIEDAD ANONIMA CERRADA","20514627852","CLIENTE",encriptaClave("20514627852"));
	u[39].inicializar("0040","CORPORACION RASTI E.I.R.L.","20517385787","CLIENTE",encriptaClave("20517385787"));
	u[40].inicializar("0041","TEXTIL SICAN S.A.C.","20600240847","CLIENTE",encriptaClave("20600240847"));
	u[41].inicializar("0042","MEDINA LINARES OFERLINDA RUTH","10074321807","CLIENTE",encriptaClave("10074321807"));
	u[42].inicializar("0043","TEXTIL ARCOIRIS S.R.L","20498647287","CLIENTE",encriptaClave("20498647287"));
	u[43].inicializar("0044","TEXTIL DORITEX EIRL","20557475398","CLIENTE",encriptaClave("20557475398"));
	u[44].inicializar("0045","TEXTIL BARAHONA E.I.R.L.","20601578281","CLIENTE",encriptaClave("20601578281"));
	u[45].inicializar("0046","CONSTRUCCIONES E INVERSIONES ALPAMA S A","20101022944","CLIENTE",encriptaClave("20101022944"));
	u[46].inicializar("0047","SURCO UTURUNCO ROGER","10457047261","CLIENTE",encriptaClave("10457047261"));
	u[47].inicializar("0048","TEXTIL GERSON ` S EIRL","20538828891","CLIENTE",encriptaClave("20538828891"));
	u[48].inicializar("0049","S.M.P. CLOTING S.R.L","20503063485","CLIENTE",encriptaClave("20503063485"));
	u[49].inicializar("0050","TEJIDOS Y CONFECCIONES DEL NORTE S.A.C.","20601433355","CLIENTE",encriptaClave("20601433355"));
	u[50].inicializar("0051","INVERSIONES TEXTILERA PERUYSA S.R.L.","20601153654","CLIENTE",encriptaClave("20601153654"));
	u[51].inicializar("0052","TEXTIL URBAN E.I.R.L.","20548614601","CLIENTE",encriptaClave("20548614601"));
	u[52].inicializar("0053","JOYALTEX E.I.R.L.","20601667615","CLIENTE",encriptaClave("20601667615"));
	u[53].inicializar("0054","TEJIDOS GLOBAL S.A.C.","20550879540","CLIENTE",encriptaClave("20550879540"));
	u[54].inicializar("0055","INDUSTRIAS KONING S.A.C.","20260929497","CLIENTE",encriptaClave("20260929497"));
	u[55].inicializar("0056","COMERCIAL TEXTIL AGUIRRE E.I.R.L.","20601689856","CLIENTE",encriptaClave("20601689856"));
	u[56].inicializar("0057","CCORAHUA HUAMAN ISIDRO","10423824021","CLIENTE",encriptaClave("10423824021"));
	u[57].inicializar("0058","MACHACA HUMPIRE ALICIA LUZMILA","10101167882","CLIENTE",encriptaClave("10101167882"));
	u[58].inicializar("0059","PC MODA S.A.C","20430772873","CLIENTE",encriptaClave("20430772873"));
	u[59].inicializar("0060","TEXTIL JEFAN`S S.A.C","20546170978","CLIENTE",encriptaClave("20546170978"));
	u[60].inicializar("0061","COMERCIALIZADORA HIROMAN S.A.C.","20508183063","CLIENTE",encriptaClave("20508183063"));
	u[61].inicializar("0062","MANUFACTURAS LA REAL S.A.","20430102631","CLIENTE",encriptaClave("20430102631"));
	u[62].inicializar("0063","TEXTIL DIAZ PONCE E.I.R.L","20514984299","CLIENTE",encriptaClave("20514984299"));
	u[63].inicializar("0064","TEXTIL JHOYER SOCIEDAD ANONIMA CERRADA","20510356315","CLIENTE",encriptaClave("20510356315"));
	u[64].inicializar("0065","HUAMANI GASPAR GUILLERMO SANATOS","10105536254","CLIENTE",encriptaClave("10105536254"));
	u[65].inicializar("0066","CREACIONES ANGELITOS S.A.C.","20491889943","CLIENTE",encriptaClave("20491889943"));
	u[66].inicializar("0067","TEXTILES MOVALTEX S.A.C.","20566518610","CLIENTE",encriptaClave("20566518610"));

	ofstream fsalida( nameFile.c_str(),ios::out|ios::binary );
	for( int i = 0 ; i < 67 ; i++ ){
		fsalida.write( u[i].convertir_cadena().c_str(), 500 );
	}

	fsalida.close();
	cout << fsalida.is_open() <<endl;
}

bool validarUsuario( string user , string rol ){
	bool valido = false;
	char tmpUser[500];
	ifstream fentrada("data/usuario.txt", ios::in | ios::binary);
	int i = 0;
	fentrada.read( tmpUser , 500 );
	vector<string> l;
	do {
		l = stringToVector(tmpUser,5);
		if( l.at(0) == user and l.at(3) == rol ){
			valido = true;
			break;
		}
		fentrada.read( tmpUser , 500 );
		i++;
	}while(!fentrada.eof());
	fentrada.close();
	return valido;
};

bool validarPassword( string user, string password ){
	bool valido = false;
	char tmpUser[500];
	ifstream fentrada("data/usuario.txt", ios::in | ios::binary);
	int i = 0;
	fentrada.read( tmpUser , 500 );
	vector<string> l;
	do {
		l = stringToVector(tmpUser,5);
		if( l.at(0) == user and l.at(4) == encriptaClave(password) ){
			valido = true;
			break;
		}
		i++;
	}while(!fentrada.eof());
	fentrada.close();
	return valido;
};


void leerPasw(char frase[])
{
    int i=0;
    cout.flush();
    do
    {
        frase[i] = (unsigned char)getch();
        if(frase[i]!=8)  // no es retroceso
        {
            cout << '*';  // muestra por pantalla
            i++;
        }
        else if(i>0)    // es retroceso y hay caracteres
        {
            cout << (char)8 << (char)32 << (char)8;
            i--;  //el caracter a borrar e el backspace
        }
        cout.flush();
    }while(frase[i-1]!=13);  // si presiona ENTER
    frase[i-1] = NULL;
    cout << endl;
}

char logearUsuario(int tipoUser){
	string nombre;
	string pass;
	string rol;
	if( tipoUser == 1 ){
		rol = "ADMIN";
	}else if( tipoUser == 2 ){
		rol = "PROVEEDOR";
	}else if( tipoUser == 3){
		rol = "CLIENTE";
	}
	char pasw[ 20 ];
	int i;
	for( i = 1 ; i < 6 ; i++ ){
		system("cls");
		mostrarCabecera();
		cout << "\t" << "Por favor ingrese su informacion de usuario" << endl;
		cout << "\t" << endl;
		cout << "\t" << "Numero de usuario "<< rol<<"?: ";
		cin >> nombre;
		if( validarUsuario( nombre , rol  )){
			cout << "\t" << "Nombre de usuario correcto, por favor ingrese su contrasena:";
			leerPasw(pasw);
			pass = pasw;
			if(validarPassword( nombre, pass )){
				cout << endl;
				cout << "\t" << "Contrasena correcta, bienvenido, sera redirigido a la interfaz  "<< rol << endl;
				system("pause>null");
				system("cls");
				if( tipoUser == 1 ){
					return interfaceAdmin(nombre);
				}else if( tipoUser == 2 ){
					return interfaceProveedor(nombre);
				}else if( tipoUser == 3){
					return interfaceCliente(nombre);
				}
			}
			else
			{
				cout << "\t" << "Contrasena incorrecta, por favor ingrese un usuario y contrasena valida de "<< rol << endl;
				system("pause>null");
			}
		}
		else
			cout << "\t" << "Nombre de usuario incorrecto, por favor ingrese su nombre de usuario nuevamente" << endl;
			system("pause>null");
		if( i > 0 ){
			cout << endl;
			cout << "\t" << "Ha realizado " << i << "/5 intentos" << endl;
			cout << endl;
			system("pause>null");
		}
		if(i == 5){
			cout << endl;
			cout << "\t" << "Ha realizado 5 intentos, por seguridad el programa se cerrara..." << endl;
			system("pause>null");
	return 's';
		}
	}
	return 'S';
}
