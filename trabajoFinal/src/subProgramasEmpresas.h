#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <string>
using namespace std;

string nombreArchivo = "aca ira el nombre del excel que aun no nos pasa la empresa";//para la estructura empresa
string nombreArchivo1="aca ira el nombre del excel que aun no nos pasa la empresa";//para la estructura materia prima
string nombreArchivo2="aca ira el nombre del excel que aun no nos pasa la empresa";//para la estructura producto terminado

struct empresas{
	string ruc_empresa;
	string nombre_empresa;
	string direccion_empresa;
	int telefono;
	string tipo;//puede ser cliente o proveedor, la cual se asignara la estructura respectiva
	string nombre_representante;//es el nombre del representante, sea cliente o proveedor, este coincidira con el nombre que se registrara
	string dni;
	string cargo;
	string correo;
	int telefono_personal;
};

struct empresas leerregistroempresa(string linea){
	stringstream tmp(linea);
	string campo;
	struct empresas emp;
	getline(tmp,emp.ruc_empresa,',');
	getline(tmp,emp.nombre_empresa,',');
	getline(tmp,emp.direccion_empresa,',');
	emp.telefono=atoi(campo.c_str());
	getline(tmp,emp.tipo,',');
	getline(tmp,emp.nombre_representante,',');
	getline(tmp,emp.dni,',');
	getline(tmp,emp.cargo,',');
	getline(tmp,emp.correo,',');
	emp.telefono_personal=atoi(campo.c_str());
	getline(tmp,campo,',');
return emp;
}

string convertircadenaemp(struct empresas empr){
	stringstream ss;
	ss<<empr.ruc_empresa<<","<<empr.nombre_empresa<<","<<empr.direccion_empresa<<","<<empr.telefono<<","<<empr.tipo<<","<<empr.nombre_representante<<","<<empr.dni<<","<<empr.cargo<<","<<empr.correo<<","<<empr.cargo<<","<<empr.telefono_personal<<endl;
return ss.str();
}

void mostrarempresas(struct empresas empresa){
	cout<<"Ruc de la empresa: "<<empresa.ruc_empresa<<endl;
	cout<<"Nombre de la empresa: "<<empresa.nombre_empresa<<endl;
	cout<<"Direccion de la empresa: "<<empresa.direccion_empresa<<endl;
	cout<<"Telefono de la empresa: "<<empresa.telefono<<endl;
	cout<<"Tipo de rol que cumple la empresa(cliente o proveedor): "<<empresa.tipo<<endl;
	cout<<"Nombre del representante de la empresa: "<<empresa.nombre_representante<<endl;
	cout<<"DNI del representante: "<<empresa.dni<<endl;
	cout<<"Cargo del representante: "<<empresa.cargo<<endl;
	cout<<"Correo del representante: "<<empresa.correo<<endl;
	cout<<"Telefono del representante: "<<empresa.telefono_personal<<endl;
}

void cargarempresas(){
	ifstream archivo(nombreArchivo.c_str());
	string linea;
	if(archivo.is_open()){
		struct empresas Emp;
		while(getline(archivo,linea)){
			Emp=leerregistroempresa(linea);
		}
	}else cout<<"Error al abrir el archivo"<<endl;
	return;
	archivo.close();
}
