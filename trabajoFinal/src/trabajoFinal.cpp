//============================================================================
// Name        : trabajoFinal.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <string>

using namespace std;

#include "util.h"
#include "subProgramasAdmin.h"
#include "subProgramasEmpresas.h"
#include "subProgramasCliente.h"
#include "subProgramasProveedor.h"
#include "subProgramasUsuario.h"



//CREACION DE ESTRUCTURAS Y ARCHIVOS - Timana Mendoza Abelardo
//como el numero de clientes y proveedores en la empresa no varia, haremos un archivo registrando las distintas empresas que actuan como
//client y/o proveedores, obteniendo asi un archivo de estos. Esto nos permitir� validar al momento de que se cree usuario y se eliga
// el rol respectivo(cliente / proveedor) ya se sabra, gracias al archivo creado, quien es cliente y proveedor y no habra confusiones.

//en la empresa clasifican la materia prima por lotes, los cuales tienen porcentajes de poliester y algodon y un peso en general.

struct materia_prima{
	string lote;
	int peso;
	float porcentaje_poliester;
	float porcentaje_algodon;
};

struct producto_terminado{
	string lote;
	string nombre;// se basan de la nomenclatura de empresas textiles
	string calidad;//en la nomenclatura de empresas textil, la empresa representa sus hilos con solo tres tipos de calidad : 30/1 , 10/1 , 24/1
	float porcentaje;//es el porcentaje de poliester negro, depende de la mezcla final, esta es la relacion entre la materia prima y el producto terminado
	int peso;
} ;


//Definicion de funciones Juan Carlos

//las rutinas Empresa se en cuentran en el archivo externo "subProgramasEmpresas.h"
string convertircadenaemp(struct empresas empr);
//las rutinas Cliente se en cuentran en el archivo externo "subProgramasCliente.h"
void almacenarempresasArchivo(struct empresas Empresa);
struct materia_prima leerregistromp(string l);
string convertircadenamp(struct materia_prima mp);
void mostrarmateriasprimas(struct materia_prima mp);
void cargarmateriasprimas();
void almacenarmateriaprimaArchivo(struct materia_prima Mp);
struct producto_terminado leerregistropt(string li);
string convertircadenapt(struct producto_terminado pt);
void mostrarproductosterminados(struct producto_terminado pt);
void cargarproductoterminado();
void almacenareproductoterminadoArchivo(struct producto_terminado pt);
void ConsultadePedidos();
char menuPrincipal();
int main() {
	
	// Menu Principal Acceso al sistema;
	//crearArchivoMateriaP();
	char resp = 's';
	//encriptaClave(clave);
	//crearArchivoUser();

    while( resp == 's' ){
	system("cls");
		mostrarCabecera();
		resp = menuPrincipal();		
		system("pause");
	}
	return 0;
}

char menuPrincipal(){
	char resp = 's';
	int eleccionMenu;
	cout << "\t1. Ingresar como Administrador"<<endl;
	cout << "\t2. Ingresar como Proveedor"<<endl;
	cout << "\t3. Ingresar como Cliente"<<endl<<endl;
	cout << "\t-> Ingrese una opcion del menu: ";
	cin >> eleccionMenu;
	
	switch( eleccionMenu ){
		case 1:
			system("cls");
			mostrarCabecera();
			logearUsuario(1);
			break;
		case 2:
			system("cls");
			mostrarCabecera();
			logearUsuario(2);
			break;
		case 3:
			system("cls");
			mostrarCabecera();
			logearUsuario(3);
			break;
		default:
		    resp = 'n';
	}
	return resp;
}

void almacenarempresasArchivo(struct empresas Empresa){
	ofstream archivo(nombreArchivo.c_str(),ios::app);
		if(archivo.is_open()){
			archivo<<convertircadenaemp(Empresa);
		}else {
			cout<<"Error al abrir archivo";
			return;
		}
		cout<<"Empleado regitrado correctamente"<<endl;
		archivo.close();
}

struct materia_prima leerregistromp(string l){
	stringstream tmp(l);
		string campo1;
		struct materia_prima mp;
		getline(tmp,mp.lote);
		mp.peso=atoi(campo1.c_str());
		mp.porcentaje_algodon=atof(campo1.c_str());
		mp.porcentaje_poliester=atof(campo1.c_str());
		getline(tmp,campo1,',');
return mp;
}
string convertircadenamp(struct materia_prima mp){
	stringstream s;
	s<<mp.lote<<","<<mp.peso<<","<<mp.porcentaje_algodon<<mp.porcentaje_poliester<<endl;
return s.str();
}
void mostrarmateriasprimas(struct materia_prima mp){
	cout<<"Nombre del lote: "<<mp.lote<<endl;
	cout<<"Peso del lote: "<<mp.peso<<endl;
	cout<<"Porcentaje de algodon que contiene el lote: "<<mp.porcentaje_algodon<<endl;//esta en discusion con el representante de la empresa si el porcentaje de algodon se tomara como vector, ya que falta confirmar si varia
	cout<<"Porcentaje de poliester que contiene el lote: "<<mp.porcentaje_poliester<<endl;
}
void cargarmateriasprimas(){
	ifstream archivo(nombreArchivo1.c_str());
	string l;
	if(archivo.is_open()){
		struct materia_prima mp;
		while(getline(archivo,l)){
			mp=leerregistromp(l);
		}
	}else cout<<"Error al abrir el archivo"<<endl;
	return;
	archivo.close();
}
void almacenarmateriaprimaArchivo(struct materia_prima Mp){
	ofstream archivo(nombreArchivo1.c_str(),ios::app);
		if(archivo.is_open()){
			archivo<<convertircadenamp(Mp);
		}else {
			cout<<"Error al abrir archivo";
			return;
		}
		cout<<"Empleado regitrado correctamente"<<endl;
		archivo.close();
}
struct producto_terminado leerregistropt(string li){
	stringstream tmp(li);
		string campo2;
		struct producto_terminado pt;
		getline(tmp,pt.lote);
		getline(tmp,pt.nombre);
		getline(tmp,pt.calidad);
		pt.porcentaje=atof(campo2.c_str());
		pt.peso=atoi(campo2.c_str());
		getline(tmp,campo2,',');
	return pt;
}
string convertircadenapt(struct producto_terminado pt){
	stringstream ss;
	ss<<pt.lote<<","<<pt.nombre<<","<<pt.calidad<<","<<pt.porcentaje<<","<<pt.peso<<","<<endl;
return ss.str();
}
void mostrarproductosterminados(struct producto_terminado pt){
	cout<<"Nombre del lote: "<<pt.lote<<endl;
	cout<<"Nombre del hilo: "<<pt.nombre<<endl;
	cout<<"Calidad del hilo "<<pt.calidad<<endl;
	cout<<"Porcentaje de poliester negro: "<<pt.porcentaje<<endl;
	cout<<"Peso: "<<pt.peso<<endl;
}
void cargarproductoterminado(){
	ifstream archivo(nombreArchivo2.c_str());
	string li;
	if(archivo.is_open()){
		struct producto_terminado pt;
		while(getline(archivo,li)){
			pt=leerregistropt(li);
		}
	}else cout<<"Error al abrir el archivo"<<endl;
	return;
	archivo.close();
}
void almacenareproductoterminadoArchivo(struct producto_terminado pt){
	ofstream archivo(nombreArchivo2.c_str(),ios::app);
		if(archivo.is_open()){
			archivo<<convertircadenapt(pt);
		}else {
			cout<<"Error al abrir archivo";
			return;
		}
		cout<<"Empleado regitrado correctamente"<<endl;
		archivo.close();
}
//Est� segunda funci�n la creo con el fin de que los clientes puedan consultar  las compras de productos terminados que ha realizado
void ConsultadePedidos(){
	cout<<"Bienvenido a TEXTIL AMAZONAS S.A.C. "<< endl;
}

