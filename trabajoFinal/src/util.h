#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <conio.h>
#include <vector>
using namespace std;

void mostrarCabecera(){
	
	cout << "*******************************************************************************"<<endl;
	cout << "*                      Bienvenido al Sistema Amazonas                         *"<<endl;
	cout << "*******************************************************************************"<<endl<<endl;
}

string encriptaClave(string nombre){
	string encriptado = "";
	
	for( int i = 0 ; i < nombre.length() ; i++ ){		
	
		encriptado += (char)( nombre[i] +  6 );
	
	} 
		
	return encriptado;
}

void printLinea(string linea,string tamanos[],int campos){
	int pos0,pos1;
	string tamano;
	pos1 = -1;

	//print cuerpo
	for( int  i = 0 ; i < campos; i++){
		pos0 = pos1 + 1;
		pos1 = linea.find(",",pos0+1);
		tamano  = "%" + tamanos[i] + "s";
		printf(tamano.c_str(), linea.substr(pos0,pos1-pos0).c_str() );
	}

	cout << endl;
}

vector<string> stringToVector(string linea,int campos) {
	vector<string> l;

	int pos0,pos1;
	pos1 = -1;
	//print cuerpo
	for( int  i = 0 ; i < campos; i++){
		pos0 = pos1 + 1;
		pos1 = linea.find(",",pos0+1);
		l.push_back(linea.substr(pos0,pos1-pos0));
	}

    return l;
}

string rayas( int tamano ){
	string rayas ;
	for( int i = 0 ; i < tamano ; i++){
		rayas += "-";
	}
	return rayas;
}
