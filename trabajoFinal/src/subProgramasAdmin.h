#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <string>
using namespace std;

struct usuario{
	string codUser;
	string nombreCompleto;
	string numDNI;
	string rol;
	string clave;
	
	void inicializar( string p1,string p2,string p3,string p4,string p5){
		codUser = p1;
		nombreCompleto = p2;
		numDNI = p3;
		rol = p4;
		clave = p5;
	}

	string convertir_cadena(){
	    return codUser+","+nombreCompleto+","+numDNI+","+rol+","+clave;
	}
};

struct materiPrima{
	char codigo[5];
	char nombre[100];
	char stock[20];
	char unidad[20];
	char descripcion[400];	
};

void crearArchivoMateriaP(){
	ofstream archivo;
	string nameFile = "data/materiaP.txt";
	archivo.open( nameFile.c_str() ,ios::out);
	struct materiPrima lista[200];
	struct materiPrima c1 = {"MP01","24/1","10","Plancha de conos","Hilo de seda 20/40"};
	struct materiPrima c2 = {"MP02","30/1","12","Plancha de conos","Tinte para tela"};
	struct materiPrima c3 = {"MP03","20/1","25","Plancha de conos","Tinte para tela"};
	struct materiPrima c4 = {"MP04","10/1","17","Plancha de conos","Tinte para tela"};
	struct materiPrima c5 = {"MP05","12/1","18","Plancha de conos","Tinte para tela"};
	struct materiPrima c6 = {"MP06","14/1","41","Plancha de conos","Tinte para tela"};

	lista[0] = c1;
	lista[1] = c2;
	lista[2] = c3;
	lista[3] = c4;
	lista[4] = c5;
	lista[5] = c6;
	
	fstream fsalida("data/materiaP.txt",ios::app|ios::binary);
	char letra;
	for(int i = 0; i < 6; i++){
		fsalida.write(lista[i].codigo, 5 );
		fsalida.write(lista[i].nombre, 100 );
		fsalida.write(lista[i].stock, 20 );
		fsalida.write(lista[i].unidad, 20 );
		fsalida.write(lista[i].descripcion, 400 );
	}
	fsalida.close();
}

string crearMP(){

	fstream fsalida("data/materiaP.txt",ios::app|ios::binary);
	char codUser[4];
	char nombreCompleto[100];
	char numDNI[20];
	char rol[20];
	char clave[400];

	cout << "Ingrese Cod. Materia prima (MPXX): ";
	cin >> codUser;
	cout << "Ingrese nombre: ";
	cin >> nombreCompleto;
	cout << "Ingrese Stock: ";
	cin >> numDNI;
	cout << "Ingrese Unidad: ";
	cin >> rol;
	cout << "Ingrese Descripcion: ";
	cin >>clave;
	fsalida.write(codUser,5);
	fsalida.write(nombreCompleto, 100 );
	fsalida.write(numDNI, 20 );
	fsalida.write(rol, 20 );
	fsalida.write(clave, 20 );

	fsalida.close();
	return "Usuario Creado!!!";
}
void mostrarMP(){
	struct materiPrima tmpCliente[100];
	ifstream fentrada("data/materiaP.txt", ios::in | ios::binary);
	int i = 0;
	string newUser;
	//cout << "\t" << "Codigo" << "\t" << "Nombre" << "\t\t"<< "Usuario" << "\t"<< "pasword" << "\n";
	printf("%10s%20s%20s%20s%20s\n", "Cod. MP","Nombre","STOCK","UNIDAD","DESCRIPCION" );
	cout << " ------------------------------------------------------------------------------------------------" << "\n";
	do {
		fentrada.read(tmpCliente[i].codigo, 5 );
		printf("%10s", tmpCliente[i].codigo );
		
		fentrada.read(tmpCliente[i].nombre, 20 );
		printf("%20s", tmpCliente[i].nombre );
		
		fentrada.read(tmpCliente[i].stock, 20 );
		printf("%20s", tmpCliente[i].stock );
		
		fentrada.read(tmpCliente[i].unidad, 20 );
		printf("%20s", tmpCliente[i].unidad );
		
		fentrada.read(tmpCliente[i].descripcion, 400 );
		printf("%20s\n", tmpCliente[i].descripcion );
		
		
		i++;
	}while(!fentrada.eof());
	
	fentrada.close();
	
	cout << "Desea ingresar nueva materia prima (S/N):";
	cin >> newUser;
	if( newUser == "S" || newUser =="s" ){
		cout << crearMP();
	}
}
void leerPasw1(char frase1[])
{
    int i=0;
    cout.flush();
    do
    {
        frase1[i] = (unsigned char)getch();
        if(frase1[i]!=8)  // no es retroceso
        {
            cout << '*';  // muestra por pantalla
            i++;
        }
        else if(i>0)    // es retroceso y hay caracteres
        {
            cout << (char)8 << (char)32 << (char)8;
            i--;  //el caracter a borrar e el backspace
        }
        cout.flush();
    }while(frase1[i-1]!=13);  // si presiona ENTER
    frase1[i-1] = NULL;
    cout << endl;
}

char *readLine(FILE *file) {
    char *line = (char*)malloc(1);
    size_t size = 0;
 
    while ((line[size] = getchar()) != '\n')
        line = (char*)realloc(line, ++size + 1);
 
    line[size] = '\0';
 
    return line;
}

string crearUsuario(){

	fstream fsalida("data/usuario.txt",ios::app|ios::binary);
	char codUser[4];
	char nombreCompleto[100];
	char numDNI[100];
	char rol[20];
	char clave[20];

	cout << "Ingrese Cod. User: ";
	cin >> codUser;
	cout << "Ingrese nombre: ";
//	string nombre;
	cin.ignore();
	cin.getline(nombreCompleto, 100);
	//scanf("%[^\n]", nombreCompleto);
	//strcpy( nombreCompleto, nombre.c_str());//
	
	cout << "Ingrese Numero Ruc: ";
	cin >> numDNI;
	cout << "Ingrese Rol: ";
	cin >> rol;
	cout << "Ingrese Clave: ";
	char pasw[ 20 ];
	char pasw1[ 20 ];
	leerPasw1(pasw);
	string encript;
	encript = encriptaClave(pasw);
	strcpy(pasw1, encript.c_str());
	fsalida.write(codUser,4);
	fsalida.write(nombreCompleto, 100 );
	fsalida.write(numDNI, 20 );
	fsalida.write(rol, 20 );
	fsalida.write(pasw1, 20 );

	fsalida.close();
	return "Usuario Creado!!!";
}

void mostrarUsuario(){
	//struct usuario tmpCliente[100];
	char tmpUsuario[500];
	ifstream fentrada("data/usuario.txt", ios::in | ios::binary);
	int i = 0;
	string newUser;

	//definiendo estructura tabla
	fentrada.read(tmpUsuario, 500 );
	string tamanos[] = {"10","45","15","15","15"};

	int campos = 5;
	printLinea("Cod. USER,Nombre,Nro. RUC,ROL,CLAVE",tamanos,campos);
	string separador;
	cout << rayas(100)<<endl;
	do {
		printLinea(tmpUsuario,tamanos,campos);
		fentrada.read(tmpUsuario, 500 );
		i++;
	}while(!fentrada.eof());
	
	fentrada.close();
	
	cout << "Desea crear un nuevo usuario (S/N):";
	cin >> newUser;
	if( newUser == "S" || newUser =="s" ){
		cout << crearUsuario();
	}
}

char menuAdmin(){
	char resp1 = 's';
	int eleccionMenu;
	
	cout << "\t1. Ver Usuarios"<<endl;
	cout << "\t2. Nuevo Usuario"<<endl;
	cout << "\t3. Ver MP"<<endl;
	cout << "\t4. Ingresar Nueva Materia prima"<<endl;
	cout << "\t5. Regresar"<<endl<<endl;
	cout << "\t-> Ingrese una opcion del menu: ";
	cin >> eleccionMenu;
	switch( eleccionMenu ){
		case 1:
			system("cls");
			mostrarCabecera();
			cout << "\t\t ROL : Admin\n\n";
			mostrarUsuario();
			break;
		case 2:
			system("cls");
			mostrarCabecera();
			cout << "\t\t ROL : Admin\n\n";
			crearUsuario();
			break;
		case 3:
			system("cls");
			mostrarCabecera();
			cout << "\t\t ROL : Admin\n\n";
			mostrarMP();
			break;
		case 4:
			system("cls");
			mostrarCabecera();
			cout << "\t\t ROL : Admin\n\n";
			crearMP();
			break;
		case 5:
			return 'r';
			break;
		default:
		    resp1 = 'n';
	}
	return resp1;
}

char interfaceAdmin(string nombre){
	char resp1 = 's';
	while( resp1 == 's' ){
		system("cls");
		mostrarCabecera();		
		cout << "\t\t ROL : Admin\n\n";
		resp1 = menuAdmin();
		if( resp1 == 'r' )
			break;
		system("pause");
	}
	return 's';
}



